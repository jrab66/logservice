from flask import Flask, request
from flask_api import status
from flask import jsonify
from flask import json
from functions import getServiceErrors,getInstanceErrors,fileUpload
import random
from waitress import serve

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "send the request to /errors to upload log \n :  curl -X POST -F file=@'log.txt' http://localhost:5000/errors"


@app.route("/errors", methods=['POST','PUT'])
def modify_filename():
    content_array = fileUpload()
    Service = getServiceErrors(content_array)
    Instance = getInstanceErrors(content_array)
    msj = "file processed succesfully "
    
    # return "hi"
    return jsonify(
        msj=msj,
        Service=Service,
        Instance=Instance
    ), 200


if __name__=="__main__":
   serve(app, listen='*:5000')