from flask import Flask, request
from werkzeug.utils import secure_filename
import sys
import os
import random

def print_to_stdout(*a): 
    # print to stdout
    print(*a, file = sys.stdout) 
  
def get_random_string(length):
    # put your letters in the following string
    sample_letters = 'abcdefghi'
    result_str = ''.join((random.choice(sample_letters) for i in range(length)))
    return result_str

def most_frequent(List): 
    counter = 0
    num = List[0] 
      
    for i in List: 
        curr_frequency = List.count(i) 
        if(curr_frequency> counter): 
            counter = curr_frequency 
            num = i 
  
    return num

def getDuplicatesWithCount(listOfElems):
    dictOfElems = dict()
    for elem in listOfElems:
        if elem in dictOfElems:
            dictOfElems[elem] += 1
        else:
            dictOfElems[elem] = 1
    dictOfElems = { key:value for key, value in dictOfElems.items() if value > 1}
    return dictOfElems

def getMaxDuplicatesWithCount(listOfElems):
    dictOfElems = dict()
    counter = 0
    for elem in listOfElems:
        if elem in dictOfElems:
            dictOfElems[elem] += 1
            counter +=1
            # print(counter)
            # print(elem)
        else:
            dictOfElems[elem] = 1
    dictOfElems = { key:value for key, value in dictOfElems.items() if value > 1}
    # print(dictOfElems)
    element=max(dictOfElems.items(), key=lambda k: k[1])
    return element


# problem specific

def fileUpload():
    file = request.files['file']
    filename=get_random_string(5) + "-" + secure_filename(file.filename)
    file.save(os.path.join('static', filename))
    print_to_stdout("filename: ",filename)
    APP_ROOT = os.path.dirname(os.path.abspath(__file__))
    APP_STATIC = os.path.join(APP_ROOT, 'static')
    #modify log
    content_array = []
    with open(os.path.join(APP_STATIC, filename)) as f:
        for line in f:

            content_array.append(line)
    # removing file from static folder
    os.remove(os.path.join('static', filename))
    return content_array

def getErrors(contentArray):
    word = "[error]"
    counter = 0
    serviceName_array = []
    instanceName_array = []
    errorName_array = []
    for i in range(len(contentArray)):
        if word in contentArray[i]:

            serviceName_array.append(contentArray[i].split(" ")[1])
            instanceName_array.append(contentArray[i].split(" ")[2])
            errorName_array.append(contentArray[i])
            
            serviceName_array[counter] = serviceName_array[counter].replace("[",'')
            instanceName_array[counter] = instanceName_array[counter].replace("]:",'')
            # print("service : ", serviceName_array[counter]," instanceid: ", instanceName_array[counter], "errors: ", contentArray[i])
            counter += 1
            print(counter)
    print(counter)

    return "done"

def getServiceErrors(contentArray):
    word = "[error]"
    counter = 0
    serviceName_array = []
    errorName_array = []
    for i in range(len(contentArray)):
        if word in contentArray[i]:
            
            # print(i)
            serviceName_array.append(contentArray[i].split(" ")[1])
            errorName_array.append(contentArray[i])
            
            serviceName_array[counter] = serviceName_array[counter].replace("[",'')
            # print("service : ", serviceName_array[counter], "errors: ", contentArray[i])
            counter += 1
    maxErrorService = getMaxDuplicatesWithCount(serviceName_array)
    # print("Service: ",most_frequent(serviceName_array),maxErrorService[1]," errors")
    print_to_stdout("Service: ",most_frequent(serviceName_array),maxErrorService[1]," errors")
    msj =  most_frequent(serviceName_array) + " " + str(maxErrorService[1]) + " errors"
    return msj

def getInstanceErrors(contentArray):
    word = "[error]"
    counter = 0
    instanceName_array = []
    errorName_array = []
    for i in range(len(contentArray)):
        if word in contentArray[i]:
            
            # print(i)
            instanceName_array.append(contentArray[i].split(" ")[2])
            errorName_array.append(contentArray[i])
            
            instanceName_array[counter] = instanceName_array[counter].replace("]:",'')
            # print(counter)
            # print(instanceName_array[counter])
            # print("instance : ", instanceName_array[counter], "errors: ", contentArray[i])
            counter += 1
    maxErrorInstance = getMaxDuplicatesWithCount(instanceName_array)
    # print("instance: ",most_frequent(instanceName_array), " errors: ", maxErrorInstance[1])
    print_to_stdout("Instance: ",most_frequent(instanceName_array),maxErrorInstance[1], " errors")
    msj = most_frequent(instanceName_array) + " " + str(maxErrorInstance[1]) + " errors"
    return msj