FROM python:3.8.3-alpine
MAINTAINER Jose Barahona "jrab6692@gmail.com"
WORKDIR /project
ADD . /project
# RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install -r requirements.txt
ENV PYTHONUNBUFFERED 1
CMD ["python","app.py"]
EXPOSE 5000