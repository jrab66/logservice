# Homework pager 

from this folder you can use this commands to test the `errorapi-pager`

#### build image 

```
 docker build -t errorapi-pager:latest .
```

#### run image 
```
docker run -p 5000:5000 errorapi-pager
```


##  testing 

#### testing outside container

Inside the container is going to be copy the file `file.log` to test using local curl.

```
curl -s -X POST -F file=@"$PWD/log.txt" http://localhost:5000/errors 

```

##### example log 
you can install `jq` to receive the json response in good format if not remove `| jq` from the next command.

```
curl -s -X POST -F file=@"$PWD/log.txt" http://localhost:5000/errors | jq
{
  "Instance": "ffd3082fe09d 15 errors",
  "Service": "api-gateway 16 errors",
  "msj": "file processed succesfully "
}

```
#### testing inside container
inside the container install `curl` and send the request.

```
docker exec -it $(docker ps | grep errorapi-pager | awk -F " " '{print $1}') sh

# apk add curl

# curl -X POST -F file=@"$PWD/log.txt" http://localhost:5000/errors 
```

example response
```
/project # curl -X POST -F file=@"$PWD/log.txt" http://localhost:5000/errors 
{"Instance":"ffd3082fe09d 15 errors","Service":"api-gateway 16 errors","msj":"file processed succesfully "}

```

### docker logs

both `instance` and `service` errors are send to `stdout` and you can check on the docker logs.

```
docker logs -f $(docker ps | grep errorapi-pager | awk -F " " '{print $1}')

```

##### example docker log 

```
docker logs -f $(docker ps | grep errorapi-pager | awk -F " " '{print $1}')
Serving on http://0.0.0.0:5000
Serving on http://[::]:5000
Service:  api-gateway 16  errors
Instance:  ffd3082fe09d 15  errors
```




###### Postdata:
Currently i use a 'static' folder to save the file to analize, probably need rework to not erase file or implement remote file storage solution.
